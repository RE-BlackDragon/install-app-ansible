## ssh

用于升级安装SSH服务，目前支持的系统有：unbuntu，centos

测试系统：ubuntu 22.0.4，Centos7(因为yum提供的库版本过低，会导致最新的ssh升级失败，建议使用gcc_mode: static安装方式)

### 升级系统自带的ssh

#### 静态编译

```yaml
- hosts: test
  vars: 
    state: 'install'
    is_internet: True
    ssh_version: '9.6p1'
    ssl_version: '3.0.13'
    zlib_version: '1.3.1'
    gcc_mode: 'static'
  roles:
  - ssh

```

#### 动态编译

这里的动态编译表示用的都是动态库，并且都是系统自带的动态库

```yaml
- hosts: test
  vars:
    state: 'install'
    is_internet: True
    ssh_version: '9.7p1'
    gcc_mode: 'share'
  roles:
  - ssh
```

### 已经使用该role升级过ssh

```yaml
- hosts: test
	vars:
    state: 'update'
    is_internet: True
    ssh_version: '9.7p1'
    ssl_version: '3.0.13'
    zlib_version: '1.3.1'
    gcc_mode: 'static'
  roles:
  - ssh
```

