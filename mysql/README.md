# mysql
实现mysql批量部署，可实现单节点部署，集群部署（主从复制，主主复制）

## 要求：
依赖ansible模块: `community.mysql.mysql_user`,使用`ansible-galaxy collection install community.mysql`命令安装。
目标主机必须存在命令pip3且可以网络下载模块或者目标主机存在python的`pymysql`模块

### 单节点部署
这里列出关键的参数配置
```yaml
is_internet: True
# 空字符串表示单节点部署
cluster_mode: ''

```
### 主从部署
```yaml
is_internet: True
cluster_mode: 'to-slave'
mysql_master: 
    replicate_db: []
    # - 'test_web'
    hosts: 
    - '192.168.0.148'
mysql_slave: 
- '192.168.0.150'
mysql_users: 
- {name: "myroot",password: "XdHk!lg6vCT",host: '%',role: '*.*:ALL,GRANT'}
mysql_log_bin: "mysql-bin"
```
### 双主部署
```yaml
is_internet: True
cluster_mode: 'to-master'
mysql_master: 
    replicate_db: []
    # - 'test_web'
    hosts: 
    - '192.168.0.148'
    - '192.168.0.150'
mysql_users: 
- {name: "myroot",password: "XdHk!lg6vCT",host: '%',role: '*.*:ALL,GRANT'}
mysql_log_bin: "mysql-bin"
```
后期可能实现：源码编译安装